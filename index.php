<?php
include "config.php";
include "function.php";
?>

<!DOCTYPE html>
<html lang="cz">
<head>
<meta charset="utf-8">
<title>Kosmonaut</title>


  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="style/main.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="fce.js"></script>

</head>

<body>

<div class="jumbotron">
  <div class="container text-center">
    <?php
        $sql = 'SELECT * FROM kosmonaut';

        $query = mysqli_query($conn, $sql);


        if (!$query) {
	     die ('SQL Error: ' . mysqli_error($conn));
        }
      ?>

    <div class="col-sm-12">
      <h1 id="header">Správce kosmonautů</h1>
      <p id="para">
          Kosmonaut nebo též astronaut či tchajkonaut je člověk, který uskutečnil kosmický let nebo absolvoval přípravu na let. Podle definice komitétu pro kosmický výzkum COSPAR se za kosmický let považuje let zahrnující nejméně jeden oběh kolem Země nebo let kosmickým prostorem trvající nejméně 90 minut.
      </p>

      <div id="box_btn" class="row">
          <a href="#popup" id="new_button"  class="btn btn-default">Nový kosmonaut</a>
          <?php
            if ($info) {
              echo $info;
            }
          ?>
      </div>

  	<table class="table table-striped table-bordered">
    		<thead>
    			<tr>

    				<th colspan="2">Jméno a Přijmení</th>
    				<th>Datum narození</th>
    				<th colspan="2">Superschopnost</th>
    			</tr>
    		</thead>
    		<tbody>
          <?php
          while( $rows = mysqli_fetch_assoc($query) ) {
          ?>
          <tr>
          <td colspan="2"><?php echo $rows["jmeno"];echo " "; echo $rows["prijmeni"]; ?></td>
          <td><?php echo $rows["datum_narozeni"]; ?></td>
          <td><?php echo $rows["super"]; ?></td>
          <td><button id="delete" class="btn btn-default" onclick="forward(<?php echo $rows["id_kos"]; ?>)">X</button></td>
          </tr>
          <?php
          };
          ?>
    </table>
    <?php
      $link = mysqli_connect($servername, $username, $password, $dbname);
      $link->query("SET NAMES utf8");
      $link->query("SET CHARACTER SET utf8");
      // Check connection
      if($link === false){
          die("ERROR: Could not connect. " . mysqli_connect_error());
      }

      if(isset($_POST['submit'])){

          // Escape user inputs for security
          $first = mysqli_real_escape_string($link, $_REQUEST['jmeno']);
          $second = mysqli_real_escape_string($link, $_REQUEST['prijmeni']);
          $born = mysqli_escape_string($link, $_REQUEST['datum_narozeni']);
          $skill = mysqli_real_escape_string($link, $_REQUEST['super']);


          // attempt insert query execution
          $sql = "INSERT INTO kosmonaut (jmeno, prijmeni, datum_narozeni, super) VALUES ('$first','$second','$born','$skill')";
          if(mysqli_query($link, $sql)){
              $info = "<p id='okmsg' class='addmsg'>Kosmonaut $first vložen v pořádku <span class='glyphicon glyphicon-ok'></span></p>";
              echo $info;

              echo("<meta http-equiv='refresh' content='2'>");
          } else{
              $info = "<p id='errmsg' class='addmsg'>Něco se při vkládání pokazilo. <span class='glyphicon glyphicon-remove'></span></p>";
              echo $info;
          };
      };
    ?>
    <div id="popup" class="insert_form">
      <form id="form" class="form-horizontal" action="index.php" method="post">
        <fieldset>
          <legend class="text-center">Nový kosmonaut</legend>

          <div class="form-group">
              <label class="col-md-3 control-label" for="jmeno">Jméno:</label>
              <div class="col-md-9">
                <input type="text" name="jmeno" id="jmeno" placeholder="Jméno kosmonauta" class="form-control">
              </div>
           </div>
          <div class="form-group">
              <label class="col-md-3 control-label" for="prijmeni">Přijmení:</label>
              <div class="col-md-9">
                <input type="text" name="prijmeni" id="prijmeni" placeholder="Přijmení kosmonauta" class="form-control">
              </div>
          </div>
          <div class="form-group">
              <label class="col-md-3 control-label" for="datum_narozeni">Datum narození:</label>
              <div class="col-md-9">
                <input type="date" name="datum_narozeni" id="datum_narozeni" placeholder="Datum narození" class="form-control">
              </div>
          </div>
          <div class="form-group">
              <label class="col-md-3 control-label" for="super">Superschopnost:</label>
              <div class="col-md-9">
                <input type="text" name="super" id="super" placeholder="Superschopnost" class="form-control">
              </div>
          </div>

          <div class="form_buttons">
            <button id="form_btn" type="submit" name="submit" class="btn btn-default" value="Vložit">Vložit</button>
            <a id="form_btn" href="#nadpis" class="btn btn-default" type="button">Zavřít</a>
          </div>

      </form>
    </div>

    </div>


  </div>
</div>

</body>
</html>
