<?php

function remove($id){
  global $conn;
  global $info;


  $result=mysqli_query($conn, "DELETE FROM kosmonaut WHERE id_kos = '$id'");
  if (!$result) {
    die('Invalid query: ' . mysql_error());
  };

  if (mysqli_affected_rows($conn)) {
    $info = "<p id='okmsg'>Smazal jsi kosmonauta s id číslem $id. <span class='glyphicon glyphicon-trash'></span></p>";
  } else {
    $info = "<p id='errmsg'>Kosmonaut nebyl nalezen. <span class='glyphicon glyphicon-search'></span></p>";
  };
}

if (isset($_GET["del"])){
    remove($_GET["del"]);
}

?>
