# README #

### Správce kosmonoutů ###
Tato portálová aplikace umožňuje správu kosmonautů, jejich editaci v podobě vkládání nových členů a mazání členů současných.

### Nastavení ###
V souboru config.php se nachází nastavení připojení k databázi, dále se zde nastavuje proměná host pro adresaci adresáře.

### Ovládání ###
Index strana zobrazuje aktální evidované kosmonauty, na každém řádku na konci se nachází tlačítko pro smazaní daného kosmonauta.

Pro vložení nového kosmonauta slouží tlačítko "Vložit nového kosmonauta"

Po vyplnění formuláře a potvrzením tlačítkem vložit se kosmonaut vloží do databáze a poté se ukáže v tabulce.

Pro odstranění kosmonauta stiskněte tlačítko s "X" na konci řádku, který chcete smazat.
